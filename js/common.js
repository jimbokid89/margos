'use strict';
if (!window.console) window.console = {};
if (!window.console.memory) window.console.memory = function() {};
if (!window.console.debug) window.console.debug = function() {};
if (!window.console.error) window.console.error = function() {};
if (!window.console.info) window.console.info = function() {};
if (!window.console.log) window.console.log = function() {};

$('.multiple-items').slick({
    infinite: true,
    arrows: false,
    verticalSwiping: true,
    vertical: true,
    dots: true
});

$('.multiple-items1').slick({
    infinite: true,
    arrows: false,
    dots: true,
    slidesToShow: 2,
    slidesToScroll: 2,
    responsive: [{
        breakpoint: 600,
        settings: {
            slidesToShow: 1,
            slidesToScroll: 1
        }
    }]
});



var a = $(window).outerHeight(),
    b = $(".header").height(),
    c = $(".b-nav-menu").height(),
    d = a - b - c - 40;
$(".fotorama_1").data("height", d);
var ww = $(window).outerWidth();
if (ww < 800) {
    $(".fotorama_1").data("height", 520)
}
if (ww < 500) {
    $(".fotorama_1").data("height", 300)
}
if (ww < 620) {
    $(".list-way").addClass("fotorama fotorama_5 white-k")
    $(".fotorama_1").data("height", 554)
    $(".list-way").data("height", 205)
}
if (ww < 621) {
    $(".fotorama_1").data("height", 300)
}

var replace = true;

$(window).on('load resize',function(){
    
    if ($(window).outerWidth() <= 1024 && replace){
            console.log('1024<');
                
           $('.js-replase .con-mob').eq(0).insertAfter($('.js-replase .con-mob').eq(2)); 
           replace = !replace;
           
    } else if ($(window).outerWidth() > 1024 && !(replace)){
        console.log('>1024');
            $('.js-replase .con-mob').eq(2).insertBefore($('.js-replase .con-mob').eq(0)); 
            replace = !replace;
    }
    $('.auto-h').matchHeight({
                byRow: true,
                property: 'height',
            });
})

$(".sow-ol").on("click", function() {
 $(".sow-con").show(); 
 $(".new-meta").show(); 
 $(".absol-m").addClass("more");
 $(".fixed-but").addClass("more_1");
});

$(".js-menu-1").on("click", function() {
    $(".sow-con").hide(); 
    $(".new-meta").hide(); 
     $(".b-right-header").removeClass("more_1"); 
     $(".b-nav-menu ").removeClass("more");
});

$(".b-tab__link-new").on("click", function() {
 $(".b-tab__link-new").removeClass("is-active"); 
 $(this).addClass("is-active");
    var tabTarget = $(this).attr('href');
    $('.some-ho-kl').removeClass('active');
    $("."+tabTarget).addClass('active');
    return false;
});


$(".b-find__but").on("click", function() {
    if ($(this).hasClass("hol")) {
        $(".footer-search").animate({width: 'toggle'},500);
            setTimeout(function(){
            $(".footer-mail").animate({width: 'toggle'},500);
            }, 500);
            $(this).removeClass("hol");
    } else {
    setTimeout(function(){
        $(".footer-search").animate({width: 'toggle'},500);
         }, 500);
            $(".footer-mail").animate({width: 'toggle'},500);
       $(this).addClass("hol");
    }
     });
    

$(function() {


    $('input[placeholder], textarea[placeholder]').placeholder();


    var langDropdown = $('.select-lang-dropdown');

    $('.select-lang').on('click', function(e) {
        e.preventDefault();
        langDropdown.slideToggle();
    });

    $(".icon-list__link-3").on("click", function(e) {
        e.preventDefault();
        $(this).toggleClass("active-close");
        $(".wrap-search").toggleClass("active-search");
    });

    $(".big-icon-search").on("click", function() {
        $(this).toggleClass("active-close");
        $(".wrap-search").toggleClass("active-search");
    });

    var tabBtn1 = $('.js-personal-tabs-s');
    tabBtn1.on('click', function(e) {
        if ($(this).hasClass('is-active')) {} else {
            tabBtn1.removeClass('is-active');
            $(this).addClass('is-active');
        }
        $('.tabl-style').hide();
        var id = $(this).attr('href');
        $(id).show();
        $('select').selectric('refresh');
        if (id == "#tabl4") {
            initMap();
        };
        return false;
    });

$(".footer-search").hide();

$('.main-che').change(function() {
    if ($('.main-che').is(':checked')) {
        $(this).siblings('.checkbox-container').children().children().prop('checked', true);
    } else {$(this).siblings('.checkbox-container').children().children().prop('checked', false);}
    });

    
    $(".move-but").on("click", function() {
        $("body,html").animate({
            scrollTop: a
        }, 500)
    }), $(".paginator__link").on("click", function() {
        $(this).parents(".paginator").find(".paginator__link").removeClass("paginator__link-active"), $(this).addClass("paginator__link-active")
    });


    var $fotoramaDiv = $('.fotorama_1').fotorama();

    var fotorama = $fotoramaDiv.data('fotorama');

    var $fotoramaDiv1 = $('.fotorama_2').fotorama();

    var fotorama1 = $fotoramaDiv1.data('fotorama');

    var $fotoramaDiv2 = $('.fotorama_3').fotorama();

    var fotorama2 = $fotoramaDiv2.data('fotorama');

    var $fotoramaDiv3 = $('.fotorama_4').fotorama();

    var fotorama3 = $fotoramaDiv3.data('fotorama');

    $(".ne").on("click", function() {
        fotorama.show('<');
    });
    $(".pr").on("click", function() {
        fotorama.show('>');
    });

    $(".ne_1").on("click", function() {
        fotorama1.show('<');
    });
    $(".pr_1").on("click", function() {
        fotorama1.show('>');
    });

    $(".ne_2").on("click", function() {
        fotorama2.show('<');
    });
    $(".pr_2").on("click", function() {
        fotorama2.show('>');
    });


    $('select').selectric({
        maxHeight: 200
    });
    var attrft = "";

    $(".show-papab").on("click", function() {
        $(".papab").addClass("papab-show");
        attrft = $(this).data('papa');
        var top = $(window).scrollTop();

        setTimeout(function() {
            $('.' + attrft).addClass("papab__form-show");
            $(".papab__form").css("top",top+50);
        }, 400);
        return false;
    });

    $(".js-papab__close").on("click", function() {
        $(".papab__form").removeClass("papab__form-show");
        setTimeout(function() {
            $(".papab").removeClass("papab-show");
        }, 400);
    });

    $('select').selectric({
        disableOnMobile: false,
        maxHeight: 200
    });


    if ($("#total") == true) {
        $('#total').text(autoplaySlider.getTotalSlideCount());
    }

    var autoplaySlider1 = $('#lightSlider').lightSlider({
        item: 1,
        pager: false,
        onBeforeSlide: function(el) {
            $('#current').text(el.getCurrentSlideCount());
        }
    });
    var autoplaySlider = $('#lightSlider_1').lightSlider({
        item: 6,
        slideMargin: 30,
        responsive: [{
            breakpoint: 800,
            settings: {
                item: 4
            }
        }]
    });

    $(".ne_4").on("click", function() {
        autoplaySlider1.goToPrevSlide();
    });
    $(".pr_4").on("click", function() {
        autoplaySlider1.goToNextSlide();
    });


    var tabBtn1 = $('.js-personal-tabs-s');
    tabBtn1.on('click', function(e) {
        if ($(this).hasClass('is-active')) {} else {
            tabBtn1.removeClass('is-active');
            $(this).addClass('is-active');
        }
        $('.tabl-style').hide();
        var id = $(this).attr('href');
        $(id).show();
        $('select').selectric('refresh');
        if (id == "#tabl4") {
            initMap();
        };
        return false;
    });


    $(".select").selectric({
        onClose: function() {
            var k = $(this).val();
            if (k == "Корпус A") {
                myLatLng = {
                    lat: 55.754862,
                    lng: 37.620712
                };
                initMap();
                $(".txt-1").show();
                $(".txt-2").hide();
            } else if (k == "Корпус B") {
                myLatLng = {
                    lat: 55.754862,
                    lng: 34.620712
                };
                initMap();
                $(".txt-2").show();
                $(".txt-1").hide();
            }
        }
    });

    var tabBtn = $('.js-personal-tabs');
    tabBtn.on('click', function(e) {
        e.preventDefault();
        if ($(this).hasClass('is-active')) {
            return;
        }
        tabBtn.removeClass('is-active');
        $(this).addClass('is-active');
        var tabTarget = $(this).attr('href');
        $('.tab-body ,.tab-info ,.fac-and-ins').removeClass('active');
        $(tabTarget).addClass('active');
    });

    $('.sidebar-items').matchHeight({
        byRow: true,
        property: 'height',
    });


    $('.top').click(function() {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });

    $(".js-menu").on("click", function() {

        if ($('.b-right-header').hasClass("fixed-but")) {
            $('.b-right-header').removeClass("fixed-but");
        } else {
            $('.b-right-header').addClass("fixed-but");
        }
        if ($('.b-nav-menu').hasClass("absol-m")) {
            $('.b-nav-menu').removeClass("absol-m");
        } else {
            $('.b-nav-menu').addClass("absol-m");
        }
    });
    $(".right-col-fot .footer__title").on("click", function() {
        $('.b-contact').slideToggle();
        $('.link-contact').slideToggle();
        $('.hide-b').slideToggle();
    });
    $(".footer__title").on("click", function() {
        $(this).siblings('.left-col-fot__list').slideToggle();
        if ($(this).hasClass("footer__title-trans")) {
            $(this).removeClass("footer__title-trans")
        } else {
            $(this).addClass("footer__title-trans")
        }
    });

    var disabledDays = [0, 3];

    $('.datepicker-here').datepicker({
        onRenderCell: function(date, cellType) {
            if (cellType == 'day') {
                var day = date.getDay(),
                    isDisabled = disabledDays.indexOf(day) != -1;

                return {
                    disabled: isDisabled
                }
            }
        }
    })

    var fbImg = $(".js-fancybox-img");

    if (fbImg.length > 0) {
        fbImg.fancybox({
            padding: 0,
            margin: 24,
            closeClick: false,
            openEffect: 'elastic',
            closeEffect: 'elastic',
            helpers: {
                title: {
                    type: 'inside'
                },
                overlay: {
                    css: {
                        'background': 'rgba(0,0,0,0.8)'
                    }
                }
            },
            afterShow: function() {
                if ($('.photoalbum').length) {
                    $(document).find('.fancybox-title').each(function() {
                        var text = $(this).text();
                        $(this).text('');
                        $(this).append('<p class="fancybox-title__text">' + text + '</p>');
                        $(this).append('<div class="photo-download"><a href="#" class="photo-download__link">Скачать оригинал<span class="i-br">JPG, 1.2 Mb</span></a></div>');
                    });
                }
            }
        });

        fbImg.each(function() {
            $(this).append('<i class="js-fancybox-img__ico"></i>');
        });
    }

    $('.list_thumb').each(function() {
        var _maxItemHeight = $(this).find('.thumb-item:first .thumb-item__col').outerHeight();

        $(this).find('.thumb-item').each(function() {
            var _itemHeight = $(this).find('.thumb-item__col').outerHeight();

            if (_itemHeight > _maxItemHeight)
                _maxItemHeight = _itemHeight;

        });

        $(this).find('.thumb-item__main, .thumb-item__period__text').height(_maxItemHeight);
    });

    $('.group-schedule__table').each(function() {
        var _maxTitleFirstHeight = $(this).find('.list_schedule-table-title .list__item:first .schedule-table-title').height(),
            _maxTitleHeight = $(this).find('.list_schedule-table-title .list__item:not(:first) .schedule-table-title').height(),
            _maxContentFirstHeight = $(this).find('.list_schedule-table-content .schedule-table-content__item:first').height(),
            _maxContentHeight = $(this).find('.list_schedule-table-content .schedule-table-content__item:not(:first)').height();


        $(this).find('.list_schedule-table-title .list__item:not(:first)').each(function() {
            var _titleHeight = $(this).find('.schedule-table-title').height();

            if (_titleHeight > _maxTitleHeight)
                _maxTitleHeight = _titleHeight;

        });

        $(this).find('.schedule-table-content').each(function() {
            $(this).find('.schedule-table-content__item:first').each(function() {
                var _contentFirstHeight = $(this).height();

                if (_contentFirstHeight > _maxContentFirstHeight)
                    _maxContentFirstHeight = _contentFirstHeight;
            });
        });

        $(this).find('.schedule-table-content').each(function() {
            $(this).find('.schedule-table-content__item:not(:first)').each(function() {
                var _contentHeight = $(this).height();

                if (_contentHeight > _maxContentHeight)
                    _maxContentHeight = _contentHeight;
            });
        });

        if (_maxTitleFirstHeight > _maxContentFirstHeight) {
            $('.schedule-table-content').each(function() {
                $(this).find('.schedule-table-content__item:first').each(function() {
                    $(this).height(_maxTitleFirstHeight);
                });
            });
        } else {
            $('.list_schedule-table-title .list__item:first .schedule-table-title').height(_maxContentFirstHeight);
        }

        if (_maxTitleHeight > _maxContentHeight) {
            $('.schedule-table-content').each(function() {
                $(this).find('.schedule-table-content__item:not(:first)').each(function() {
                    $(this).height(_maxTitleHeight);
                });
            });
            $('.list_schedule-table-title .list__item .schedule-table-title:not(:first)').height(_maxTitleHeight);
        } else {
            $('.list_schedule-table-title .list__item .schedule-table-title:not(:first)').height(_maxContentHeight);
            $('.schedule-table-content').each(function() {
                $(this).find('.schedule-table-content__item:not(:first)').each(function() {
                    $(this).height(_maxContentHeight);
                });
            });
        }

    });


    if ($('.container-thumb-slider').length) {
        var thumb = $('.list_thumb');
        thumb.each(function() {
            if ($(this).find('.list__item').length > 1) {
                var thumb = $(this).bxSlider({
                    hideControlOnEnd: true,
                    infiniteLoop: true,
                    easing: 'easeOutElastic',
                    pagerCustom: '.list_thumb-nav',
                    prevText: 'Предыдущий период',
                    nextText: 'Следующий период',
                    nextSelector: '.list_thumb__next',
                    prevSelector: '.list_thumb__prev',
                    /* auto: true,
                     autoStart: true,
                     autoHover: true,
                     speed: 1000,
                     pause: 9000,*/
                    onSlideBefore: function(currentSlide, indexOld, indexNew) {
                        $('.list_thumb-nav .list__item').removeClass('list__item_active');
                        $('.list_thumb-nav .list__item').eq(indexNew).toggleClass('list__item_active');
                    },
                    onSliderLoad: function(currentIndex) {
                        $('.list_thumb .thumb-item').on('click', function() {
                            var thumbIndex = $(this).data("slide");
                            thumb.goToSlide(thumbIndex);

                            $('.gallery-slider__preview .gallery-slider-item').removeClass('i-hide');
                            $('.gallery-slider__preview .gallery-slider-item').addClass('gallery-slider-item_animate');
                            $('.gallery-slider__preview .gallery-slider-item').not(':eq(' + thumbIndex + ')').addClass('i-hide');
                            $('.gallery-slider__preview .gallery-slider-item').not(':eq(' + thumbIndex + ')').removeClass('gallery-slider-item_animate');
                        });

                        $('.list_thumb').each(function() {
                            var _widthItem = 0;
                            $(this).find('.thumb-item').each(function() {
                                _widthItem += $(this).outerWidth();
                            });
                            $(this).css('min-width', _widthItem + 1);
                        });
                    },

                    onSlideNext: function($slideElement, oldIndex, newIndex) {
                        $('.gallery-slider__preview .gallery-slider-item').removeClass('i-hide');
                        $('.gallery-slider__preview .gallery-slider-item').addClass('gallery-slider-item_animate');
                        $('.gallery-slider__preview .gallery-slider-item').not(':eq(' + newIndex + ')').addClass('i-hide');
                        $('.gallery-slider__preview .gallery-slider-item').not(':eq(' + newIndex + ')').removeClass('gallery-slider-item_animate');
                    },
                    onSlidePrev: function($slideElement, oldIndex, newIndex) {
                        $('.gallery-slider__preview .gallery-slider-item').removeClass('i-hide');
                        $('.gallery-slider__preview .gallery-slider-item').addClass('gallery-slider-item_animate');
                        $('.gallery-slider__preview .gallery-slider-item').not(':eq(' + newIndex + ')').addClass('i-hide');
                        $('.gallery-slider__preview .gallery-slider-item').not(':eq(' + newIndex + ')').removeClass('gallery-slider-item_animate');
                    }
                });
            }
        });

        var preview = $('.gallery-slider');
        preview.each(function() {
            if ($(this).find('.gallery-slider__item').length > 1) {
                var preview = $(this).bxSlider({
                    mode: 'fade',
                    easing: 'easeOutElastic',
                    hideControlOnEnd: true,
                    infiniteLoop: true,
                    pager: false,
                    prevText: 'Предыдущая дата',
                    nextText: 'Следующая дата',
                    onSliderLoad: function(currentIndex) {
                        $('.list_period .period-item').on('click', function() {
                            var thumbIndex = $(this).data("slide");
                            preview.goToSlide(thumbIndex);
                        });
                    }
                });
            }
        });


        $('.gallery-slider__preview .gallery-slider-item').addClass('i-hide');
        $('.gallery-slider__preview .gallery-slider-item').eq(0).removeClass('i-hide').addClass('gallery-slider-item_animate');

        $('.list_thumb-nav .thumb-item-nav').on('click', function() {
            var thumbIndex = $(this).data("slide-index");

            $('.gallery-slider__preview .gallery-slider-item').removeClass('i-hide');
            $('.gallery-slider__preview .gallery-slider-item').addClass('gallery-slider-item_animate');
            $('.gallery-slider__preview .gallery-slider-item').not(':eq(' + thumbIndex + ')').addClass('i-hide');
            $('.gallery-slider__preview .gallery-slider-item').not(':eq(' + thumbIndex + ')').removeClass('gallery-slider-item_animate');
        });
    }

    /*$('.list_period').each(function(){
        var periodCounter = $(this).find('.list__item').length;
        $(this).find('.list__item').each(function(){
            var periodIndex = $(this).index();
            $(this).addClass('list__item_' + (periodCounter - periodIndex));
        });
    });*/

    var $tabContainer = $('.schedule__call');
    $tabContainer.find('.schedule__call__preview .schedule__call__type').each(function(i) {

        $('.schedule__call__item').hide();
        $('.schedule__call__item:first').show();

        $(this).click(function() {

            if ($(this).hasClass('schedule__call__type_selected')) return false;

            $tabContainer.find('.schedule__call__type').removeClass('schedule__call__type_selected');
            $(this).addClass('schedule__call__type_selected');

            $tabContainer.find('.schedule__call__item:visible').hide().removeClass('schedule__call__item_active');
            $tabContainer.find('.schedule__call__item').eq(i).fadeIn().addClass('schedule__call__item_active');

        });
    });

    $('.js-schedule-table-content').each(function() {
        if ($(this).find('.list__item').length > 1) {
            console.log('go');
            var slider = $(this).bxSlider({
                slideWidth: 174,
                minSlides: 1,
                maxSlides: 7,
                moveSlides: 1,
                slideMargin: 0,
                infiniteLoop: true,
                hideControlOnEnd: true,
                speed: 700,
                pager: false,
                easing: 'easeInCubic',
                prevText: 'Предыдущая день',
                nextText: 'Следующая день'
            });
        };
    });

    $(window).resize(function() {
        $('.list_thumb').each(function() {
            var _maxItemHeight = $(this).find('.thumb-item:first .thumb-item__col').outerHeight();

            $(this).find('.thumb-item').each(function() {
                var _itemHeight = $(this).find('.thumb-item__col').outerHeight();

                if (_itemHeight > _maxItemHeight)
                    _maxItemHeight = _itemHeight;

            });

            $(this).find('.thumb-item__main, .thumb-item__period__text').height(_maxItemHeight);
        });

        $('.list_thumb').each(function() {
            var _widthItem = 0;
            $(this).find('.thumb-item').each(function() {
                _widthItem += $(this).outerWidth();
            });
            $(this).css('min-width', _widthItem + 1);
        });

        $('.group-schedule__table').each(function() {
            var _maxTitleFirstHeight = $(this).find('.list_schedule-table-title .list__item:first .schedule-table-title').height(),
                _maxTitleHeight = $(this).find('.list_schedule-table-title .list__item:not(:first) .schedule-table-title').height(),
                _maxContentFirstHeight = $(this).find('.list_schedule-table-content .schedule-table-content__item:first').height(),
                _maxContentHeight = $(this).find('.list_schedule-table-content .schedule-table-content__item:not(:first)').height();


            $(this).find('.list_schedule-table-title .list__item:not(:first)').each(function() {
                var _titleHeight = $(this).find('.schedule-table-title').height();

                if (_titleHeight > _maxTitleHeight)
                    _maxTitleHeight = _titleHeight;

            });

            $(this).find('.schedule-table-content').each(function() {
                $(this).find('.schedule-table-content__item:first').each(function() {
                    var _contentFirstHeight = $(this).height();

                    if (_contentFirstHeight > _maxContentFirstHeight)
                        _maxContentFirstHeight = _contentFirstHeight;
                });
            });

            $(this).find('.schedule-table-content').each(function() {
                $(this).find('.schedule-table-content__item:not(:first)').each(function() {
                    var _contentHeight = $(this).height();

                    if (_contentHeight > _maxContentHeight)
                        _maxContentHeight = _contentHeight;
                });
            });

            if (_maxTitleFirstHeight > _maxContentFirstHeight) {
                $('.schedule-table-content').each(function() {
                    $(this).find('.schedule-table-content__item:first').each(function() {
                        $(this).height(_maxTitleFirstHeight);
                    });
                });
            } else {
                $('.list_schedule-table-title .list__item:first .schedule-table-title').height(_maxContentFirstHeight);
            }

            if (_maxTitleHeight > _maxContentHeight) {
                $('.schedule-table-content').each(function() {
                    $(this).find('.schedule-table-content__item:not(:first)').each(function() {
                        $(this).height(_maxTitleHeight);
                    });
                });
                $('.list_schedule-table-title .list__item .schedule-table-title:not(:first)').height(_maxTitleHeight);
            } else {
                $('.list_schedule-table-title .list__item .schedule-table-title:not(:first)').height(_maxContentHeight);
                $('.schedule-table-content').each(function() {
                    $(this).find('.schedule-table-content__item:not(:first)').each(function() {
                        $(this).height(_maxContentHeight);
                    });
                });
            }

        });
    });
});


var myLatLng = {
    lat: 55.754862,
    lng: 37.620712
};

function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 12,
        center: myLatLng
    });

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: 'Hello World!'
    });
}



$('input[name="daterange"]').daterangepicker({

    "locale": {
        "format": "DD.MM.YYYY",
        "separator": " - ",
        "applyLabel": "Выбрать",
        "cancelLabel": "Отменить",
        "fromLabel": "С",
        "toLabel": "По",
        "customRangeLabel": "Custom",
        "daysOfWeek": [
            "Пн",
            "Вт",
            "Ср",
            "Чт",
            "Пт",
            "Сб",
            "Вс"
        ],
        "monthNames": [
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Май",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь"
        ],
        "firstDay": 1
    }
});


$('.i-calendar').click(function() {
    $(this).parent().find('input').click();
});

$('#startDate').daterangepicker({
    singleDatePicker: true,
    startDate: moment().subtract(6, 'days')
});

$('#endDate').daterangepicker({
    singleDatePicker: true,
    startDate: moment()
});

$('#daterange').on('cancel.daterangepicker', function(ev, picker) {
    $('#daterange').val('');
});


$('.js-daterange-drop').on('click', function(event) {
    event.preventDefault();
    $('#daterange').val('');
});




$(document).ready(function() {
    $('.faq-que-block .list-content').hide();
});



$('.faq-que-block .list-title').on('click', function(e) {
    e.preventDefault();
    var self = $(this),
        item = self.parents('.item'),
        content = item.find('.list-content'),
        contents = $('.faq-que-block .list-content'),
        items = $('.faq-que-block .item');


    if (item.hasClass('active')) {
        item.removeClass('active');
        content.hide();
    } else {
        items.removeClass('active');
        contents.hide();
        item.addClass('active');
        content.show();
    }
});

var ios = (/iphone|ipad|ipod/i.test(navigator.userAgent.toLowerCase()));
$( window ).load(function() {
if (ios){
    $('html').addClass('ios');
}});
(function($) {
    $(function() {
        $('.formstyler').styler();
    });
})(jQuery);


$(document).ready(function(){
$.fn.equivalent = function (){
    var $blocks = $(this),
        maxH    = $blocks.eq(0).height();
    $blocks.each(function(){
        maxH = ( $(this).height() > maxH ) ? $(this).height() : maxH;
    });
    $blocks.height(maxH+15);
    }
$('.st-reviews__item').equivalent();
});


var heightSlide = 0;
var heightSlide1 = 0;
$(window).on('load resize',function(){
    
    if ($(window).outerWidth() > 500){
        $('.multiple-items').on('afterChange', function(event, slick, currentSlide){
            heightSlide = $(".slick-active .st-reviews__right").height();
           $('.multiple-items').height(heightSlide+33);
        }); 
    } else {
        $('.multiple-items').on('afterChange', function(event, slick, currentSlide){
            heightSlide = $(".slick-active .st-reviews__right").height();
            heightSlide1 = $(".slick-active .st-reviews__img").height();
           $('.multiple-items').height(heightSlide+heightSlide1+30);
        }); 
    }
});